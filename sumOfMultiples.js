function SumOfMultiples(finalNumber, firstInput, secondInput) {
  let sumOfSubsequentNumbersOf1stInput = 0;
  let subsequentNumberOf1stInput = 0;
  let sumOfSubsequentNumbersOf2ndInput = 0;
  let subsequentNumberOf2ndInput = 0;

  while (subsequentNumberOf1stInput < finalNumber - firstInput) {
    subsequentNumberOf1stInput += firstInput;
    sumOfSubsequentNumbersOf1stInput += subsequentNumberOf1stInput;
  }
  while (subsequentNumberOf2ndInput < finalNumber - secondInput) {
    subsequentNumberOf2ndInput += secondInput;
    if (subsequentNumberOf2ndInput % firstInput === 0) continue;
    sumOfSubsequentNumbersOf2ndInput += subsequentNumberOf2ndInput;
  }
  return sumOfSubsequentNumbersOf1stInput + sumOfSubsequentNumbersOf2ndInput;
}

console.log("result", SumOfMultiples(1000, 3, 5));
